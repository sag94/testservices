BROKER SCHEMA EDMLogging

DECLARE QUEUE_MANAGER EXTERNAL CHARACTER '';
DECLARE project_Name CHARACTER 'RTLCRM_';


CREATE COMPUTE MODULE Error_MessageLogger_Compute
	CREATE FUNCTION Main() RETURNS BOOLEAN
	BEGIN
		SET OutputRoot.Properties = InputRoot.Properties;
		SET OutputRoot.MQMD = InputRoot.MQMD;
		SET OutputRoot.XMLNSC = NULL;
		DECLARE messageCode CHARACTER '';
	    
		IF(Environment.Variables.EDM.MSGTYPE = 'E') THEN
			
			SET OutputRoot.XMLNSC.DETAILS.MSGTYPE = 'E';
			SET OutputRoot.XMLNSC.DETAILS.RETRY_COUNT = COALESCE(Environment.Variables.EDM.RETRY_COUNT,'51');	
			SET OutputRoot.XMLNSC.DETAILS.SOURCE_QUEUE = Environment.Variables.EDM.SOURCE_QUEUE;
			SET OutputRoot.XMLNSC.DETAILS.PAYLOAD = Environment.Variables.EDM.PAYLOAD; 
			SET OutputRoot.XMLNSC.DETAILS.MSG_FLOW_NAME = MessageFlowLabel;
			SET OutputRoot.XMLNSC.DETAILS.APPL_NAME =  COALESCE(Environment.Variables.EDM.APPL_NAME,'');
			SET OutputRoot.XMLNSC.DETAILS.QUEUE_MANAGER = QueueManagerName;
			SET OutputRoot.XMLNSC.DETAILS.MESSAGE_ID = COALESCE(Environment.Variables.REFID,'') || '-' || CAST(CURRENT_TIMESTAMP AS CHARACTER FORMAT 'yyyy-MM-dd HH:mm:ss');
			IF( Environment.Variables.EDM.VALIDATION_ERRORCODE IS NOT NULL AND Environment.Variables.EDM.VALIDATION_ERRORCODE <> '') THEN
				SET OutputRoot.XMLNSC.DETAILS.ERROR_CODE = Environment.Variables.EDM.VALIDATION_ERRORCODE;
				SET Environment.Variables.ERRORSTACK = Environment.Variables.EDM.VALIDATION_EXCEPTIONTEXT;
				SET Environment.Variables.ERRORSTACKCODE = Environment.Variables.EDM.VALIDATION_ERRORCODE;
				-- Temporary Code for verifying MessageCode in EDM DB, will be removed
				IF(Environment.Variables.EDM.MSGCODE IS NULL OR Environment.Variables.EDM.MSGCODE = '') THEN
					SET OutputRoot.XMLNSC.DETAILS.EXCEPTION_TEXT = 'MessageCodeMissing' || '-' || Environment.Variables.EDM.VALIDATION_EXCEPTIONTEXT;
				ELSE 
					SET OutputRoot.XMLNSC.DETAILS.EXCEPTION_TEXT = Environment.Variables.EDM.VALIDATION_EXCEPTIONTEXT;
				END IF;
				SET OutputRoot.XMLNSC.DETAILS.MSGCODE = COALESCE(Environment.Variables.EDM.MSGCODE,'');
			ELSEIF (Environment.Variables.EDM.ERRORMESSAGE IS NOT NULL AND Environment.Variables.EDM.ERRORMESSAGE <> '') THEN
				SET Environment.Variables.ERRORSTACK = Environment.Variables.EDM.ERRORMESSAGE;
				SET Environment.Variables.ERRORSTACKCODE = Environment.Variables.EDM.ERRORCODE;
				SET OutputRoot.XMLNSC.DETAILS.ERROR_CODE = COALESCE(Environment.Variables.EDM.ERRORCODE,'');
				-- Temporary Code for verifying MessageCode in EDM DB, will be removed
				IF(Environment.Variables.EDM.MSGCODE IS NULL OR Environment.Variables.EDM.MSGCODE = '') THEN
					SET OutputRoot.XMLNSC.DETAILS.EXCEPTION_TEXT = 'MessageCodeMissing' || '-' ||  COALESCE(Environment.Variables.EDM.ERRORMESSAGE,'');
				ELSE
					SET OutputRoot.XMLNSC.DETAILS.EXCEPTION_TEXT = COALESCE(Environment.Variables.EDM.ERRORMESSAGE,'');
				END IF;
				SET OutputRoot.XMLNSC.DETAILS.MSGCODE = COALESCE(Environment.Variables.EDM.MSGCODE,'');
			ELSE 
				DECLARE errorCode char;
				DECLARE errorMessage char;
				DECLARE errorContext char;
				DECLARE userGeneratedExceptionYN char;	
				CALL getLastExceptionDetail(InputExceptionList,errorCode,errorMessage,errorContext,userGeneratedExceptionYN);
				SET Environment.Variables.ERRORSTACK = errorMessage;
				SET Environment.Variables.ERRORSTACKCODE = errorCode;
				SET OutputRoot.XMLNSC.DETAILS.ERROR_CODE = errorCode;
				SET OutputRoot.XMLNSC.DETAILS.EXCEPTION_TEXT = COALESCE(Environment.Variables.EDM.ERRORMESSAGE, '') ||''|| errorMessage;
				IF(Environment.Variables.EDM.MsgCode IS NOT NULL AND Environment.Variables.EDM.MsgCode <> '') THEN
					SET messageCode = Environment.Variables.EDM.MSGCODE;
				ELSE
					CALL setDefaultMessageCodes(OutputRoot.XMLNSC.DETAILS, Environment.Variables.EDM);
				END IF;
			END IF;
		
		ELSE
			SET OutputRoot.XMLNSC.DETAILS.SOURCE_QUEUE = Environment.Variables.EDM.SOURCE_QUEUE;
		    SET OutputRoot.XMLNSC.DETAILS.PAYLOAD = Environment.Variables.EDM.PAYLOAD;		
			SET OutputRoot.XMLNSC.DETAILS.MSGTYPE = Environment.Variables.EDM.MSGTYPE;	 
			SET OutputRoot.XMLNSC.DETAILS.MSG_FLOW_NAME = MessageFlowLabel;
			SET OutputRoot.XMLNSC.DETAILS.APPL_NAME =  COALESCE(Environment.Variables.EDM.APPL_NAME,'');
			SET OutputRoot.XMLNSC.DETAILS.QUEUE_MANAGER = QueueManagerName;
			SET OutputRoot.XMLNSC.DETAILS.MESSAGE_ID =  COALESCE(Environment.Variables.REFID,'')||'-'||COALESCE(Environment.Variables.EDM.MSG_DESC,'')|| '-' || CAST(CURRENT_TIMESTAMP AS CHARACTER FORMAT 'yyyy-MM-dd HH:mm:ss');	
		END IF;
		
		SET Environment.Variables.EDM = NULL;
		RETURN TRUE;
	END;

END MODULE;


CREATE PROCEDURE getLastExceptionDetail(IN InputTree REFERENCE,
OUT errorCode char,
OUT errorMessage char,
OUT errorContext char,
OUT userGeneratedExceptionYN char)	
    /***********************************************************************************
	 * A procedure that will get the details of the last exception from a message
	 * IN   InputTree:  The incoming exception list
	 * 			-- (in a Compute node use InputExceptionList)
	 * OUT  errorCode:  The error code from the user generated exception and message number for the other types
	 * OUT  errorMessage : The error message from the user generated exception and concatenantion of all insert[*].text 
	 *                     for other types . 
	 * The code to be used for throwing user defined exception should be
	 * THROW USER EXCEPTION SEVERITY 3 VALUES ( out_status_code, out_status_desc);
	 * OUT  errorContext : This is the concatenation of label ( which tells from which node 
	 *                     the error was triggered)  and line no of the code.
	 * OUT userGeneratedExceptionYN - Y if userGenerated , N if not
	 ***********************************************************************************/
   BEGIN
   	    -- Create a reference to the first child of the exception list
   	    DECLARE ptrExceptionNext REFERENCE TO InputTree.*[1];
   	    DECLARE ptrException REFERENCE TO InputTree.*[1];
   	    DECLARE brokerExceptionMsg CHARACTER  ' ';
   	    -- keep looping while the moves to the child of exception list work 
		WHILE ptrException.Number IS NOT NULL DO
			-- store the current values for the error number and text
			
  			-- now move to the last child which should be the next exceptionlist
  			---SET ptrException = ptrExceptionNext;
  			MOVE ptrException LASTCHILD;
  			
		END WHILE; 
		
		MOVE ptrException PARENT;
		SET errorContext =ptrException.Label|| ';'||CAST(ptrException.Line AS CHARACTER);
		IF ptrException.Text = 'User generated exception' THEN
			
			SET userGeneratedExceptionYN ='Y';
			SET errorCode = ptrException.Insert[1].Text;
			SET errorMessage = ptrException.Insert[2].Text;
		ELSE
			FOR insertRef AS ptrException.Insert[] DO
					SET brokerExceptionMsg = brokerExceptionMsg || ';' || insertRef.Text;
			END FOR;
			IF ptrException.Text IS NOT NULL THEN
				SET brokerExceptionMsg = ptrException.Text||';'||brokerExceptionMsg;
			END IF; 
			SET errorCode = 'MBException'||':'||CAST(ptrException.Number AS CHARACTER);
			SET errorMessage = brokerExceptionMsg;
			SET userGeneratedExceptionYN ='N';
		END IF;
		
   END;
   
   CREATE PROCEDURE setDefaultMessageCodes ( INOUT outref_Details REFERENCE,IN env_var REFERENCE )
   BEGIN
   	DECLARE messageCode CHARACTER '';
   		IF CONTAINS(env_var.ERRORSTACK, 'A schema validation error') OR CONTAINS(env_var.ERRORSTACK, 'XML parsing error has occurred') THEN
			SET messageCode = project_Name||  COALESCE(env_var.APP_CODE, '') ||'_1002';            -- XML Parsing Error
		ELSE 
			SET messageCode = project_Name||  COALESCE(env_var.APP_CODE,'') ||'_1001';             -- Default Error, MB Exception 
		END IF;
		SET outref_Details.MSGCODE = messageCode;
   END;